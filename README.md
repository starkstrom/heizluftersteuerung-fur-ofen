# Heizlüftersteuerung für Ofen, version 0.1 #


### Wofür ist das Programm? ###

* Das Programm steuert den Heizlüfter für den Ofen.
* Weitere Infos zur Benutzung, o.ä.: https://confluence.starkstrom-augsburg.de/display/CH/How+to+Ofen


### Issues ###

* Problem: Es ist unmöglich abzuschätzen wie lange der Ofen für x-Grad aufheizt.
* Lösung: Es sind zwingend weitere Tests notwendung um den Ofen zwecks Wärmeerhaltung und Wirkungsgrad zu untersuchen.
* Aber: Das aktuelle Programm ist **STABIL!**
* Das heißt: Die langsame Temperaturerhöhung funktioniert und der Ofen hält die Endtemperatur bis zum Ende der eingestellten Gesamtzeit.