int sensorValue;
float endTemperature;
float isTemperature;
float tempIncreasePerHour;
float tempIncreasePerMinute; 
float timeOverall;
int runtime = 0;
float targetTemp;
float Hysterese = 0.5;                //+- Grad Celsius

void setup() {
  Serial.begin(115200);
  pinMode(2, OUTPUT);
  setupVariables();
  //For the first start nessecary
  updateTemperature();
  targetTemp = isTemperature + tempIncreasePerMinute;
}

void loop() {
  increaseTemperature();
}

void increaseTemperature(){ 
  //Stop when runtime reached input-time
  while(runtime <= (timeOverall*60)){
  updateTemperature();
  Serial.println();
  if ((isTemperature < targetTemp - Hysterese) && (isTemperature < endTemperature - Hysterese)) {
    activateFoehn();
  } else if ((isTemperature > targetTemp + Hysterese) || (isTemperature > endTemperature + Hysterese)) {
    deactivateFoehn();
  }
  runtime++;
  if (runtime % 60 == 0) {
    targetTemp += tempIncreasePerMinute; 
  }
  delay(1000);
  printInfo();
  }
  deactivateFoehn();
  //Stop program
  Serial.println("Finished with temperature: ");
  Serial.println(isTemperature);
  while(true){
      Serial.print("Temperatur: ");
      Serial.println(isTemperature);
  }
}
void printInfo() {
Serial.print("Temperatur: ");
Serial.print(isTemperature);
Serial.print("; Soll-Temperatur: ");
Serial.print(targetTemp);
Serial.print("; Föhn-Status: ");
Serial.print(digitalRead(2));
Serial.print("; Verbleibende Zeit (min): ");
Serial.println(timeOverall - runtime/60);
  
}

void updateTemperature(){
  readSensorValue();
  isTemperature = (0.275 * sensorValue)-80.784;
  //Serial.println(isTemperature);
}

void setupVariables(){
  Serial.println();
  Serial.println("Please type in your end-temperature!");
  while(Serial.available() == 0){
   
  }
  endTemperature = Serial.parseFloat();
  Serial.print("Your selected end-temperature is: ");
  Serial.print(endTemperature);
  Serial.println();
  delay(1000);
  Serial.println("Please type in your max. temp-increase per hour!");
  while(Serial.available() == 0){
   
  }
  tempIncreasePerHour = Serial.parseInt();
  Serial.print("Your selected max. temp-increase per hour: ");
  Serial.print(tempIncreasePerHour);
  Serial.println();
  delay(1000);
  Serial.println("Please type in the overall-time! (minutes)");
  while(Serial.available() == 0){
   
  }
  timeOverall = Serial.parseInt();
  Serial.print("The overall-time is: ");
  Serial.print(timeOverall);
  Serial.println();
  delay(1000);
  calculateIncrease();
}

void calculateIncrease(){
  //Example: 10 degrees per hour = 10/60 degrees per minute
  tempIncreasePerMinute = tempIncreasePerHour/60;
  delay(500);
  Serial.print("Temp-Increase-Cap per 30sek is: ");
  Serial.println(tempIncreasePerMinute, 5);
  delay(3000);
}

void countSeconds(){
  runtime++;
  //Serial.println(runtime);
  delay(1000);
}
void readSensorValue(){
  sensorValue = analogRead(A0);
}

void printTemperature(){
  Serial.print("Current temperature: ");
  Serial.print(isTemperature);
  //Serial.println();
}

void activateFoehn(){
  digitalWrite(2, HIGH);
  //Serial.println("Foehn activated!");
}

void deactivateFoehn(){
  digitalWrite(2, LOW);
  //Serial.println("Foehn deactivated!");
}
